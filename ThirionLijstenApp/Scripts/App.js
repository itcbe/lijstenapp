﻿'use strict';

var context = SP.ClientContext.get_current();
var user = context.get_web().get_currentUser();
var DefaultTarief = 0;
var JaakTarief = 0;
var IsabelleTarief = 0;
var drempelwaarde;
var registraties = [];


// This code runs when the DOM is ready and creates a context object which is needed to use the SharePoint object model
$(document).ready(function () {
    SetDatePickers();
    var drempel = GetDrempelwaarde();
    drempel.done(function () {
        getUserName();
    })
});

function SetDatePickers() {
    var today = new Date();
    var currentyear = today.getFullYear();
    var currentmonth = today.getMonth();
    var firstdate = new Date(currentyear, currentmonth, 1);
    var lastdate = new Date(currentyear, currentmonth, daysInMonth(currentmonth, currentyear));
    $.datepicker.setDefaults($.datepicker.regional["nl"]);
    $(".datepickers").datepicker({
        'dateFormat': 'dd/mm/yy'
    });
    $('#vandatum').val(ToDateString(firstdate, true));
    $('#totdatum').val(ToDateString(lastdate, true));
}

    // This function prepares, loads, and then executes a SharePoint query to get the current users information
    function getUserName() {
        context.load(user);
        context.executeQueryAsync(onGetUserNameSuccess, onGetUserNameFail);
    }

    // This function is executed if the above call is successful
    // It replaces the contents of the 'message' element with the user name
    function onGetUserNameSuccess() {
        $('#message').text('Hello ' + user.get_title());
        GetSpecialeTarieven();
    }

    // This function is executed if the above call fails
    function onGetUserNameFail(sender, args) {
        alert('Failed to get user name. Error:' + args.get_message());
    }

    function FilterButton_Click() {
        var klant = "";
        if ($("#klantselect > option:selected").text() != "-- Alle klanten --")
            klant = $("#klantselect > option:selected").text()
        var opdracht = "";
        if ($("#opdrachtselect > option:selected").text() != "-- Alle opdrachten --")
            opdracht = $("#opdrachtselect > option:selected").text();
        var medewerker = "";
        if ($("#medewerkerselect > option:selected").text() != "-- Alle medewerkers --")
            medewerker = $("#medewerkerselect > option:selected").text();
        var activiteit = "";
        if ($("#activiteitselect > option:selected").text() != "-- Alle activiteiten --")
            activiteit = $("#activiteitselect > option:selected").text()

        var van = "";
        if ($("#vandatum").val() != "") {
            van = ToSharePointDate($("#vandatum").val());
        }
        var tot = "";
        if ($("#totdatum").val() != "") {
            tot = ToSharePointDate($("#totdatum").val());
        }
        registraties = [];
        GetTijdsregistraties(klant, opdracht, medewerker, activiteit, van, tot, 0);
    }

    function PrintButton_Click() {
        //Popup($('#printablediv').html());
        $('#printablediv').print();
    }

    function Popup(data) {
        var printdata = '<html><head><title>' + $('#klantselect > option:selected').text() + '</title>';
        printdata += '<link rel="stylesheet" href="../Content/App.css" type="text/css" />';
        printdata += '<style>body{font-family:"Century Gothic"; }</style>';
        printdata += '</head><body class="PrintDiv">';
        printdata += data;
        printdata += '</body></html>';

        printdata.print();

        //var mywindow = window.open('', '', 'height=400,width=600');
        //mywindow.document.write('<html><head><title>' + $('#klantselect > option:selected').text() + '</title>');
        //mywindow.document.write('<link rel="stylesheet" href="../Content/App.css" type="text/css" />');
        //mywindow.document.write("<style>body{font-family:'Century Gothic'; }</style>");
        //mywindow.document.write('</head><body class="PrintDiv">');
        //mywindow.document.write(data);
        //mywindow.document.write('</body></html>');

        ////mywindow.document.close(); // necessary for IE >= 10
        ////mywindow.focus(); // necessary for IE >= 10

        //mywindow.print();
        //mywindow.close();

        return true;
    }


