﻿function GetTijdsregistraties(klant, opdracht, medewerker, activiteit, van, tot, startwaarde) {
    var self = this;

    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Tijdsregistraties');
        var currentdate = new Date();
        if (van == "")
            van = currentdate.getFullYear() + "-" + ((currentdate.getMonth() + 1) < 10 ? "0" + (currentdate.getMonth() + 1) : (currentdate.getMonth() + 1)) + "-01T00:00:00";
        if (tot == "")
            tot = currentdate.getFullYear() + "-" + ((currentdate.getMonth() + 1) < 10 ? "0" + (currentdate.getMonth() + 1) : (currentdate.getMonth() + 1)) + "-" + daysInMonth(currentdate.getMonth() + 1, currentdate.getFullYear()) +"T00:00:00";

        self.eindwaarde = startwaarde + 4990;

        var caml = "<View>";
        caml += "<Query>";
        caml += "<Where>";
        if (klant != "")
            caml += "<And>";
        if (opdracht != "")
            caml += "<And>";
        if (medewerker != "")
            caml += "<And>";
        if (activiteit != "")
            caml += "<And>";
        caml += "<And><And><And>";
        caml += "<Geq><FieldRef Name='ID' /><Value Type='Counter'>" + startwaarde + "</Value></Geq>";
        caml += "<Lt><FieldRef Name='ID' /><Value Type='Counter'>" + self.eindwaarde + "</Value></Lt>";
        caml += "</And>";
        caml += "<Geq><FieldRef Name='Startuur' /><Value Type='DateTime'>" + van + "</Value></Geq>";
        caml += "</And>";
        caml += "<Leq><FieldRef Name='Einduur' /><Value Type='DateTime'>" + tot + "</Value></Leq>";
        caml += "</And>";
        if (klant != "") {
            caml += "<Eq><FieldRef Name='Klant' /><Value Type='Lookup'>" + klant + "</Value></Eq>";
            caml += "</And>";
        }
        if (opdracht != "") {
            caml += "<Eq><FieldRef Name='Opdracht' /><Value Type='Choice'>" + opdracht + "</Value></Eq>";
            caml += "</And>";
        }
        if (medewerker != "") {
            caml += "<Eq><FieldRef Name='Author' /><Value Type='User'>" + medewerker + "</Value></Eq>";
            caml += "</And>";
        }
        if (activiteit != "") {
            caml += "<Eq><FieldRef Name='Activiteit' /><Value Type='Lookup'>" + activiteit + "</Value></Eq>";
            caml += "</And>";
        }

        caml += "</Where>";
        caml += "<OrderBy><FieldRef Name='Startuur' /></OrderBy>";
        caml += "</Query>";
        caml += "<ViewFields>";
        caml += "<FieldRef Name='Klant' />";
        caml += "<FieldRef Name='Startuur' />";
        caml += "<FieldRef Name='Author' />";
        caml += "<FieldRef Name='Activiteit' />";
        caml += "<FieldRef Name='Title' />";
        caml += "<FieldRef Name='Omschrijving' />"
        caml += "<FieldRef Name='Duur' />";
        caml += "<FieldRef Name='Activiteit_x003a_Tarief' />"
        caml += "</ViewFields>";
        caml += "</View>";

        var query = new SP.CamlQuery();
        query.set_viewXml(caml);

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
            Function.createDelegate(self, self._onLoadListSucceeded),
            Function.createDelegate(self, self._onLoadListFailed)
            );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var arraylist = [];
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();
            var mymedewerker = item.get_item('Author').get_lookupValue();
            var myactiviteit = "";
            if (item.get_item('Activiteit') !== null)
                myactiviteit = item.get_item('Activiteit').get_lookupValue();
            var mytarief = "";
            if (item.get_item('Activiteit_x003a_Tarief') !== null)
                mytarief = item.get_item('Activiteit_x003a_Tarief').get_lookupValue();
            registraties.push({
                Datum: item.get_item('Startuur'),
                Klant: item.get_item('Klant').get_lookupValue(),
                Van: SharePointDatestringToDateString(van),
                Tot: SharePointDatestringToDateString(tot),
                Medewerker: mymedewerker.split(" ")[0],
                Activiteit: myactiviteit,
                Omschrijving: item.get_item('Omschrijving'),
                Duur: item.get_item('Duur'),
                Tarief: mytarief
            })
        }
        var maxwaarde = parseInt(drempelwaarde) + 4990;
        if (maxwaarde > self.eindwaarde) {
            startwaarde = self.eindwaarde;
            GetTijdsregistraties(klant, opdracht, medewerker, activiteit, van, tot, startwaarde)
        }
        else {
            if (klant === "")
                BindAlleKlanten(registraties);
            else
                BindKlantLijst(registraties);
        }
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Unable to load file list: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function BindKlantLijst(arraylist) {
    var div = $("#printablediv");
    div.empty();
    div.append("<p>" + $("#klantselect > option:selected").text() + "</p>")
    if (arraylist.length > 0) {
        div.append("<p> Van: " + arraylist[0].Van + " tot: " + arraylist[0].Tot+ ".</p>")

        var totaalduur = 0;
        var totaalKost = 0;
        var tabel = "<table><thead><tr><th style='margin:0; padding:3px 5px;'></th>";
        tabel += "<th style='margin:0; padding:3px 5px;'></th>";
        tabel += "<th style='margin:0; padding:3px 5px;'></th>";
        tabel += "<th style='margin:0; padding:3px 5px;'>Omschrijving</th>";
        tabel += "<th style='margin:0; padding:3px 5px;'>Uren</th>";
        tabel += "<th style='margin:0; padding:3px 5px;'>Eénheidsprijs</th>";
        tabel += "<th style='margin:0; padding:3px 5px;'>Omzet</th>";
        tabel += "</tr></thead><tbody><tr><td style='border-bottom: 1px solid black; margin:0; padding:0;' colspan='7' height='3'></td></tr>";

        for (var i = 0; i < arraylist.length; i++) {
            var duur = parseFloat(arraylist[i].Duur);
            var omschrijving = "";
            if (arraylist[i].Omschrijving != null)
                omschrijving = arraylist[i].Omschrijving;
            var tarief = parseFloat(arraylist[i].Tarief);
            if (isNaN(tarief))
                tarief = 62.5;

            //test voor tarief jaak en isabelle
            if (arraylist[i].Medewerker === "Jaak Thirion" || arraylist[i].Medewerker === "Jaak")
                tarief = JaakTarief;
            else if (arraylist[i].Medewerker === "isabelle" && (arraylist[i].Activiteit === "Fiscaliteit" || arraylist[i].Activiteit === "Accountancy"))
                tarief = IsabelleTarief;
            //einde test voor jaak en isabelle

            var subtotaal = duur * tarief;
            tabel += "<tr><td style='margin:0; padding:3px 5px;'>" + ToDateString(arraylist[i].Datum, true) + "</td>";
            tabel += "<td style='margin:0; padding:3px 5px;'>" + arraylist[i].Medewerker + "</td>";
            tabel += "<td style='margin:0; padding:3px 5px;'>" + arraylist[i].Activiteit + "</td>";
            tabel += "<td style='margin:0; padding:2px 5px; min-width:300px;'>" + omschrijving + "</td>";
            tabel += "<td style='margin:0; padding:3px 5px;'>" + duur.toFixed(3) + "</td>";
            tabel += "<td style='margin:0; padding:3px 5px;'>" + tarief.toFixed(3) + "</td>";
            tabel += "<td style='margin:0; padding:3px 5px;'>" + subtotaal.toFixed(3) + "</td></tr>";
            totaalduur = parseFloat(totaalduur) + duur;
            totaalKost = parseFloat(totaalKost) + subtotaal;
        }
        tabel += "<tr><td style='border-top: 2px solid black; margin:0; padding:0;' colspan='7' height='3'></td></tr>";
        tabel += "<tr >";
        tabel += "<td style='margin:0; padding:3px 5px;'><b>Totaal</b></td>";
        tabel += "<td style='margin:0; padding:3px 5px;'></td>";
        tabel += "<td style='margin:0; padding:3px 5px;'></td>";
        tabel += "<td style='margin:0; padding:3px 5px;'></td>";
        tabel += "<td style='margin:0; padding:3px 5px;'><b>" + totaalduur.toFixed(3) + "</b></td>";
        tabel += "<td style='margin:0; padding:3px 5px;'></td>";
        tabel += "<td style='margin:0; padding:3px 5px;'><b>&euro;" + totaalKost.toFixed(3) + "</b></td>";
        tabel += "</tr>";
        tabel += "</tbody>";
        tabel += "<tfoot>";       
        tabel += "</tfoot></table>";
        div.append(tabel);
    }
    else {
        div.append("<p>Geen resultaten gevonden.</p>");
    }
}

function BindAlleKlanten(arraylist) {
    var div = $("#printablediv");
    div.empty();
    div.append("<p>" + $("#klantselect > option:selected").text() + "</p>")
    arraylist.sort(function compare(a, b) {
        if (a.Klant < b.Klant)
            return -1;
        if (a.Klant > b.Klant)
            return 1;
        return 0;
    });
    if (arraylist.length > 0) {
        div.append("<p> Van: " + arraylist[0].Van + " tot: " + arraylist[0].Tot + ".</p>")

        var totaalduur = 0;
        var totaalKost = 0;
        var tabel = "<table><thead><tr><th style='margin:0; padding:5px 9px;'></th>";
        tabel += "<th style='margin:0; padding:3px 5px;'></th>";
        tabel += "<th style='margin:0; padding:3px 5px;'></th>";
        tabel += "<th style='margin:0; padding:3px 5px;'>Omschrijving</th>";
        tabel += "<th style='margin:0; padding:3px 5px;'>Uren</th>";
        tabel += "<th style='margin:0; padding:3px 5px;'>Eénheidsprijs</th>";
        tabel += "<th style='margin:0; padding:3px 5px;'>Omzet</th>";
        tabel += "</tr></thead><tbody><tr><td style='border-bottom: 1px solid black; margin:0; padding:0;' colspan='7' height='3'></td></tr>";
        var currentklant = arraylist[0].Klant;
        var klantindex = 0;
        var klantduur = 0;
        var klantbedrag = 0;
        for (var i = 0; i < arraylist.length; i++) {
            if (i === 0) {
                tabel += "<th><td colspan='7' style='margin-left:10px;'><b>" + currentklant + "</b></td></th>";
            }
            else if (arraylist[i].Klant !== currentklant) {
                currentklant = arraylist[i].Klant;
                tabel += "<tr><td style='border-bottom: 1px solid black; margin:0; padding:0;' colspan='7' height='3'></td></tr>";
                tabel += "<tr style='color:#0171c5'><td>Totaal klant</td><td></td><td></td><td></td><td style='margin:0; padding:5px 9px;'><b>" + klantduur.toFixed(2) + "</b></td><td></td><td style='margin:0; padding:5px 9px;'><b>&euro;" + klantbedrag.toFixed(2) + "</b></td></tr>";
                tabel += "<tr><td style='border-bottom: 1px solid black; margin:0; padding:0;' colspan='7' height='3'></td></tr>";
                tabel += "<th><td colspan='7' style='margin-left:10px;'><b>" + currentklant + "</b></td></th>";
                klantduur = 0;
                klantbedrag = 0;
            }
            var duur = parseFloat(arraylist[i].Duur);
            var omschrijving = "";
            if (arraylist[i].Omschrijving != null)
                omschrijving = arraylist[i].Omschrijving;
            var tarief = parseFloat(arraylist[i].Tarief);
            if (isNaN(tarief))
                tarief = DefaultTarief;
            //test voor tarief jaak en isabelle

            if (arraylist[i].Medewerker === "Jaak Thirion" || arraylist[i].Medewerker === "Jaak")
                tarief = JaakTarief;
            else if (arraylist[i].Medewerker === "isabelle" && (arraylist[i].Activiteit === "Fiscaliteit" || arraylist[i].Activiteit === "Accountancy"))
                tarief = IsabelleTarief;
            //einde test voor jaak en isabelle
            var subtotaal = duur * tarief;

            tabel += "<tr><td style='margin:0; padding:3px 5px;'>" + ToDateString(arraylist[i].Datum, true) + "</td>";
            tabel += "<td style='margin:0; padding:3px 5px;'>" + arraylist[i].Medewerker + "</td>";
            tabel += "<td style='margin:0; padding:3px 5px;'>" + arraylist[i].Activiteit + "</td>";
            tabel += "<td style='margin:0; padding:3px 5px; min-width:300px;'>" + omschrijving + "</td>";
            tabel += "<td style='margin:0; padding:3px 5px;'>" + duur.toFixed(3) + "</td>";
            tabel += "<td style='margin:0; padding:3px 5px;'>" + tarief.toFixed(3) + "</td>";
            tabel += "<td style='margin:0; padding:3px 5px;'>" + subtotaal.toFixed(3) + "</td></tr>";
            klantduur = parseFloat(klantduur) + duur;
            klantbedrag = parseFloat(klantbedrag) + subtotaal;
            totaalduur = parseFloat(totaalduur) + duur;
            totaalKost = parseFloat(totaalKost) + subtotaal;
        }
        tabel += "<tr><td style='border-bottom: 1px solid black; margin:0; padding:0;' colspan='7' height='3'></td></tr>";
        tabel += "<tr style='color:#0171c5'><td>Totaal klant</td><td></td><td></td><td></td><td style='margin:0; padding:3px 5px;'><b>" + klantduur.toFixed(3) + "</b></td><td></td><td style='margin:0; padding:5px 9px;'><b>&euro;" + klantbedrag.toFixed(3) + "</b></td></tr>";
        tabel += "<tr><td style='border-bottom: 1px solid black; margin:0; padding:0;' colspan='7' height='3'></td></tr>";

        tabel += "<tr><td style='border-top: 2px solid black; margin:0; padding:0;' colspan='7' height='3'></td></tr>";
        tabel += "<tr>";

        tabel += "<td style='margin:0; padding:3px 5px;'><b>Totaal</b></td>";
        tabel += "<td style='margin:0; padding:3px 5px;'></td>";
        tabel += "<td style='margin:0; padding:3px 5px;'></td>";
        tabel += "<td style='margin:0; padding:3px 5px;'></td>";
        tabel += "<td style='margin:0; padding:3px 5px;'><b>" + totaalduur.toFixed(2) + "</b></td>";
        tabel += "<td style='margin:0; padding:3px 5px;'></td>";
        tabel += "<td style='margin:0; padding:3px 5px;'><b>&euro;" + totaalKost.toFixed(2) + "</b></td></tr>";
        tabel += "</tbody>";
        tabel += "<tfoot>";
        tabel += "</tfoot></table>";
        div.append(tabel);
    }
    else {
        div.append("<p>Geen resultaten gevonden.</p>");
    }
}

function GetOpdrachten() {
    var self = this;

    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        this.list = hostWebContext.get_web().get_lists().getByTitle('Tijdsregistraties');
        this.fields = list.get_fields();

        this.opdrachten = clientContext.castTo(list.get_fields().getByInternalNameOrTitle("Opdracht"), SP.FieldChoice);

        //var caml = "<View>";
        //caml += "<ViewFields>";
        //caml += "<FieldRef Name='Opdracht' />";
        //caml += "</ViewFields>";
        //caml += "</View>";

        //var query = new SP.CamlQuery();
        //query.set_viewXml(caml);

        //self._pendingItems = list.getItems(query);
        //clientContext.load(self._pendingItems);
        clientContext.load(list);
        clientContext.load(fields);
        clientContext.load(opdrachten);
        clientContext.executeQueryAsync(
            Function.createDelegate(self, self._onLoadListSucceeded),
            Function.createDelegate(self, self._onLoadListFailed)
            );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var context = new SP.ClientContext.get_current();
        // Converting the Field to SPFieldChoice from the execution results
        var myChoicesfield = context.castTo(this.fields.getByInternalNameOrTitle("Opdracht"), SP.FieldChoice);
        //get_choices() method will return the array of choices provided in the field

        var choices = myChoicesfield.get_choices();

        var opdrachtselect = $("#opdrachtselect");
        opdrachtselect.empty();
        opdrachtselect.append("<option>-- Alle opdrachten --</option>");
        if (choices.length > 0) {

            for (var i = 0; i < choices.length; i++) {
                opdrachtselect.append("<option>" + choices[i] + "</option>");
            }
        }
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Unable to load file list: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}


