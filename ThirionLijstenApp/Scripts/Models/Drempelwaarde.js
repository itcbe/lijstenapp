﻿function GetDrempelwaarde() {
    var dfd = $.Deferred(function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Drempelwaarde');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View><ViewFields><FieldRef Name='Title' /></ViewFields></View>");

        var _pendingItems = list.getItems(query);
        clientContext.load(_pendingItems);
        clientContext.executeQueryAsync(function () {
            var listEnumerator = _pendingItems.getEnumerator();
            var arraylist = [];
            while (listEnumerator.moveNext()) {
                var item = listEnumerator.get_current();
                drempelwaarde = item.get_item('Title');
            }
            dfd.resolve();
        },
            function (sender, args) {
                alert("Fout bij het laden van de drempelwaarde!\n" + args.get_message());
                dfd.reject();
            }
        );

    });
    return dfd.promise();
}