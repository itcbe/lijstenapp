﻿function GetMedewerkers() {
    var self = this;

    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var collGroup = clientContext.get_web().get_siteGroups();
        var oGroup = collGroup.getByName('Medewerkers');
        this.collUser = oGroup.get_users();
        clientContext.load(collUser);

        clientContext.executeQueryAsync(
        Function.createDelegate(self, self._onLoadListSucceeded),
        Function.createDelegate(self, self._onLoadListFailed)
        );

    }

    self._onLoadListSucceeded = function (sender, args) {
        var userInfo = '';
        var userEnumerator = collUser.getEnumerator();
        var arraylist = [];
        while (userEnumerator.moveNext()) {
            var oUser = userEnumerator.get_current();

            arraylist.push(
            {
                ID: oUser.get_id(),
                Titel: oUser.get_title(),
                LoginName: oUser.get_loginName(),
                Email: oUser.get_email()
            });
        }
        BindMedewerkers(arraylist);
        GetOpdrachten();
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Kan de medewerkers niet laden: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function BindMedewerkers(arraylist) {
    $("#medewerkerselect").empty()
    $("#medewerkerselect").append("<option>-- Alle medewerkers --</option>");
    for (var i = 0; i < arraylist.length; i++) {
        var option = "<option value='" + arraylist[i].ID + "'>" + arraylist[i].Titel + "</option>";
        $("#medewerkerselect").append(option);
    }
}