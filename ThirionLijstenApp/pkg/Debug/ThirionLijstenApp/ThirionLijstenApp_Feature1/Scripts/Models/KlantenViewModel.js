﻿function GetKlanten() {
    var self = this;

    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Klanten');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View>"
                        + "<Query>"
                        + "<OrderBy><FieldRef Name='Bedrijfsnaam' /></OrderBy>"
                        + "</Query>"
                        + "<ViewFields>"
                        + "<FieldRef Name='ID' />"
                        + "<FieldRef Name='Bedrijfsnaam' />"
                        + "</ViewFields>"
                        + "</View>");

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
            Function.createDelegate(self, self._onLoadListSucceeded),
            Function.createDelegate(self, self._onLoadListFailed)
            );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var arraylist = [];
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();
            arraylist.push({
                ID: item.get_item('ID'),
                Bedrijfsnaam : item.get_item('Bedrijfsnaam')
            })
        }
        BindKlantenToSelect(arraylist);
        GetMedewerkers();
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Unable to load file list: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function BindKlantenToSelect(arraylist) {
    var klantenselect = $("#klantselect");
    klantenselect.empty();
    klantenselect.append("<option>-- Alle klanten --</option>")
    if (arraylist.length > 0) {
        for (var i = 0; i < arraylist.length; i++) {
            klantenselect.append("<option>" + arraylist[i].Bedrijfsnaam + "</option>");
        }
    }
}