﻿function GetActiviteiten() {
    self = this;
    self.loadList = function () {
        var clientContext = SP.ClientContext.get_current();
        var hostWebContext = new SP.AppContextSite(clientContext, getQueryStringParameter("SPHostUrl"));
        var list = hostWebContext.get_web().get_lists().getByTitle('Activiteiten');

        var query = new SP.CamlQuery();
        query.set_viewXml("<View>"
                   + "<ViewFields>"
                      + "<FieldRef Name='Title' />"
                      + "<FieldRef Name='Tarief' />"
                   + "</ViewFields>"
                 + "</View>");

        self._pendingItems = list.getItems(query);
        clientContext.load(self._pendingItems);
        clientContext.executeQueryAsync(
        Function.createDelegate(self, self._onLoadListSucceeded),
        Function.createDelegate(self, self._onLoadListFailed)
        );
    }

    self._onLoadListSucceeded = function (sender, args) {
        var listEnumerator = self._pendingItems.getEnumerator();
        var arraylist = [];
        while (listEnumerator.moveNext()) {
            var item = listEnumerator.get_current();

            arraylist.push(
            {
                Omschrijving: item.get_item('Title'),
                Tarief: item.get_item('Tarief').toString()
            });
        }
        SetActiviteiten(arraylist);
        GetKlanten();
    }

    self._onLoadListFailed = function (sender, args) {
        alert('Unable to load file list: ' + args.get_message() + '\n' + args.get_stackTrace());
    }

    self.loadList();
}

function SetActiviteiten(arraylist) {
    var activiteiten = $("#activiteitselect");
    activiteiten.empty();
    activiteiten.append("<option>-- Alle activiteiten --</option>");
    if (arraylist.length > 0) {
        for (var i = 0; i < arraylist.length; i++) {
            activiteiten.append("<option>" + arraylist[i].Omschrijving + "</option>")
        }
    }
}