﻿<%-- The following 4 lines are ASP.NET directives needed when using SharePoint components --%>

<%@ Page Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage, Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" MasterPageFile="~masterurl/default.master" Language="C#" %>

<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%-- The markup and script in the following Content element will be placed in the <head> of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
    <link href="../Content/jquery-ui.min.css" rel="stylesheet" />
    <link href="../Content/jquery-ui.structure.min.css" rel="stylesheet" />
    <link href="../Content/jquery-ui.theme.min.css" rel="stylesheet" />
    <script type="text/javascript" src="../Scripts/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../Scripts/datepicker-nl.js"></script>
    <script type="text/javascript" src="/_layouts/15/sp.runtime.js"></script>
    <script type="text/javascript" src="/_layouts/15/sp.js"></script>
    <script type="text/javascript" src="../Scripts/DatumFuncties.js"></script>
    <script type="text/javascript" src="../Scripts/Models/SpecialeTarievenModel.js"></script>
    <script type="text/javascript" src="../Scripts/Models/KlantenViewModel.js"></script>
    <script type="text/javascript" src="../Scripts/Models/ActiviteitenViewModel.js"></script>
    <script type="text/javascript" src="../Scripts/Models/TijdsregistratieViewModel.js"></script>
    <script type="text/javascript" src="../Scripts/Models/MedewerkerViewModel.js"></script>
    <meta name="WebPartPageExpansion" content="full" />

    <!-- Add your CSS styles to the following file -->
    <link rel="Stylesheet" type="text/css" href="../Content/App.css" />

    <!-- Add your JavaScript to the following file -->
    <script type="text/javascript" src="../Scripts/App.js"></script>
</asp:Content>

<%-- The markup in the following Content element will be placed in the TitleArea of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server">
    Lijsten App
</asp:Content>

<%-- The markup and script in the following Content element will be placed in the <body> of the page --%>
<asp:Content ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <div id="wrapper">
        <div>
            <p id="message">
                <!-- The following content will be replaced with the user name when you run the app - see App.js -->
                initializing...
            </p>
        </div>
        <h3>Filter opties</h3>
        <div id="filterdiv">           
            <div class="leftdiv">
                <p>
                    <label for="klantselect">Klant:</label><br />
                    <select id="klantselect">
                        <option>-- Alle klanten --</option>
                    </select>
                </p>
                <p>
                    <label for="opdrachtselect">Opdracht:</label><br />
                    <select id="opdrachtselect">
                        <option>-- Alle opdrachten --</option>
                    </select>
                </p>
                <p>
                    <label for="medewerkerselect">Medewerker:</label><br />
                    <select id="medewerkerselect">
                        <option>-- Alle medewerkers --</option>
                    </select>
                </p>
            </div>
            <div class="leftdiv">
                <p>
                    <label for="activiteitselect">Activiteit:</label><br />
                    <select id="activiteitselect">
                        <option>-- Alle activiteiten --</option>
                    </select>
                </p>
                <p>
                    <label for="vandatum">Van:</label><br />
                    <input id="vandatum" type="text" class="datepickers" />
                </p>
                <p>
                    <label for="totdatum">Tot:</label><br />
                    <input id="totdatum" type="text" class="datepickers" />
                </p>
            </div>
            <div class="clearLeft buttonDiv">
                <input type="button" value="Filter" id="FilterButton" onclick="FilterButton_Click();" />
            </div>
        </div>
        <h3>Resultaten</h3>
        <div id="printablediv">

        </div>
        <div class="buttonDiv">
            <input id="PrintButton" value="Print" type="button" onclick="PrintButton_Click();" />
        </div>

    </div>


</asp:Content>
